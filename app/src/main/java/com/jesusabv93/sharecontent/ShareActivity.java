package com.jesusabv93.sharecontent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

public class ShareActivity extends AppCompatActivity {
    public static final String TAG = ShareActivity.class.getSimpleName();

    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    @BindView(R.id.btnShare) AppCompatButton btnShare;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_share);
        ButterKnife.bind(this);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override public void onSuccess(Sharer.Result result) {
                Toast.makeText(ShareActivity.this, "Se publicó", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onSuccess: "+result.toString());
            }

            @Override public void onCancel() {
                Toast.makeText(ShareActivity.this, "Se canceló", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onCancel: ");

            }

            @Override public void onError(FacebookException error) {
                Toast.makeText(ShareActivity.this, "Hubo un error", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onError: "+error.toString());
                error.printStackTrace();

            }
        });
    }

    @OnClick(R.id.btnShare) public void onViewClicked() {
        ShareLinkContent content = new ShareLinkContent.Builder().setContentUrl(
            Uri.parse("https://www.facebook.com/7catsartstudio/"))
            .setContentTitle("7 Cats Art Studio")
            .setContentDescription("Un comic que cambiará tu manera de ver las historias")
            .setImageUrl(Uri.parse("http://www.xtreme-soft.com/capitulos/es/capitulo1/Cap1LOW.jpg"))
            .build();


        if (ShareDialog.canShow(ShareLinkContent.class)){
            shareDialog.show(content);
        }

    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
